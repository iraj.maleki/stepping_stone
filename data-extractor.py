import numpy as np
import os
import glob

os.system('cat *-iter/OUTPUT* > all_itaration_data.txt')

all_itaration_data = np.loadtxt("all_itaration_data.txt")
#print(all_itaration_data.shape)
all_folders =[name for name in os.listdir(".") if os.path.isdir(name)]
#print(all_folders)



with open('all_itaration_data.txt') as f:
    header_name = f.readline()

file_name = glob.glob('03-iter/*.txt')

with open('all_itaration_data.txt') as f:
    header_name = f.readline()

Mean = np.mean(all_itaration_data , axis = 0)
VARIANCE = np.var(all_itaration_data , axis = 0)

np.savetxt("MEAN-{}".format(file_name[0][8:]), Mean, fmt='%f',header="{}-CALC=MEAN".format(header_name))

np.savetxt("VARIANCE-{}".format(file_name[0][8:]), VARIANCE, fmt='%f',header="{}-CALC=VARIANCE".format(header_name))



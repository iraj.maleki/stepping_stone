#!/bin/bash


module load compilers/gcc-5.1.0
gcc steaping_stone_iter_V3.c

N_folder=$1
echo "NUMBER OF FOLDERS =  $N_folder"

read -p "Do you confirm? [y/n]: " confirm

if [[ $confirm != 'y' ]]
then
    echo ""
    echo "Exit!"
    echo ""
    exit 0
fi
echo "CREATING FOLDERS..."

for i in $(eval echo {01..$N_folder});
do
	echo "$i"
	mkdir $i-iter
	cp ./a.out $i-iter
	cp JOB.sh $i-iter/JOB-$i-iter.sh
	cd $i-iter
	my_var="$i-iter"
	sed -i "2s/.*/#PBS -N $my_var/" JOB-$i-iter.sh
	qsub JOB-$i-iter.sh
	sleep 5
	cd ..
done
	

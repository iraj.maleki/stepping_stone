#include <stdio.h>  
#include <stdlib.h> 
#include <time.h> 
#include <string.h>
#include <time.h>


int N_particle = 100;       //Number of particles
int len_cells = 200000;     //length oe number of Cells
int t = 150;              // number of evolution time steps
float P_m_0 = 1.0;          //
float P_0= 0.2;
double P_g;
float Kessy = 0.5;
int iteration = 10;        //Number of iteretions 
    


int main(){

	clock_t begin = clock();

	FILE * fp;
	// Allocates storage
	char *OUTPUTNAME = (char*)malloc(200 * sizeof(char));
	// Prints "Hello world!" on OUTPUTNAME
	sprintf(OUTPUTNAME, "./OUTPUT-N=%d-t=%d-iteration=%d-kessy=%.2f-P_m_0=%.2f-P_0=%.2f.txt",N_particle, t,iteration,Kessy,P_m_0,P_0);  
	/* open the file for writing*/
	fp = fopen (OUTPUTNAME,"w");
	fprintf(fp, "#N=%d  t=%d  iteration=%d  kessy=%.2f  P_m_0=%.2f  P_0=%.2f\n",N_particle, t,iteration,Kessy,P_m_0,P_0 );

	srand(time(0));  ///find the best place for this!!!

	for(int iter=1;iter <= iteration;iter++)
	
	{
		printf("iteration is = %d of %d\n", iter, iteration);
	    /////////////////////////////////////////////
		int cells[len_cells];
		memset( cells, 0, len_cells*sizeof(int) );
		cells[0] = N_particle;
		cells[1] = N_particle;
	
		//////////////////////
		float sum_of_all_cells_list[t];
		memset( sum_of_all_cells_list, 0, t*sizeof(float) );
		//////////////////////
		float P_m[len_cells];
		P_m[0] = 0.0;
		memset( P_m, 0, len_cells*sizeof(float) );
	
		for(int i=1;i<len_cells;i++)
		{
			P_m[i] = (-0.5 + (float)rand()/(float)(RAND_MAX)) * Kessy + P_m_0;
		}
		
		int cellindex;
		int cell;
		
	
		for(int time_step=0; time_step<t; time_step++)
		{
			//printf("Time Step is = %d\n",time_step);
		
			//for (int i=0; i<50; i++)
			//{
			/* Passing addresses of array elements*/
			//	printf("%d  ", cells[i]);
			//}
			//printf("\nEND OF FFIRST CEEEEEEEELL\n");
	
	
			int non_zero_cells_index[len_cells];
			memset( non_zero_cells_index, 0, len_cells*sizeof(int) );
	
			int non_zero_cells_sum=0;
			int counter = 0;
	
			for(int cell=0;cell<len_cells;cell++)
	
				if (cells[cell] > 0)
				{
					non_zero_cells_index[counter] = cell;
					non_zero_cells_sum += cells[cell];
					counter += 1;
				}
			//printf("counter             -> %d\n",counter);
			//printf("non_zero_cells_sum  -> %d\n",non_zero_cells_sum);
	
			//printf("counter = %d\n",counter );		
	
			for(int particle=0;particle < non_zero_cells_sum; particle ++)
			{
	
				cellindex = rand()%(counter+1); 
				cell = non_zero_cells_index[cellindex];
				//printf("selected cell is -> %d\n",cell );
	
				//check generation
				
				P_g = P_0 * (1.0 - (double)(cells[cell]) / (double)N_particle);
	
				//printf("P_g is the -> %f\n",P_g );
	
				float Random_generation_probablity = ((float)rand()/(float)(RAND_MAX)) * P_0;
	
				//printf("Random_generation_probablity -> %f\n",Random_generation_probablity );
	
	
				if(Random_generation_probablity < (float)P_g)
				{
					//printf("Cell is accepted for generation\n");
					cells[cell] += 1;
				}
				//printf("Now the cell is the -> %d\n", cells[cell] );
				//then check migration
	
				if(((float)rand()/(float)(RAND_MAX)) < P_m_0)
				{
					//allowed for migration
					//printf("Cell is accepted for migration\n");
	
					float Random_migration_probablity = (float)rand()/(float)(RAND_MAX);
	
					if (Random_migration_probablity < (P_m[cell] / (P_m[cell] + P_m[cell+1])))
					{
						//go left
						if (cells[cell-1] < N_particle)
						{
							cells[cell] -= 1;
							cells[cell-1] += 1;
							//printf("Particle foes to the left\n");
						}
					}
	
					if (Random_migration_probablity > (P_m[cell] / (P_m[cell] + P_m[cell+1])))
					{
						//go right
						if (cells[cell+1] < N_particle)
						{
							cells[cell] -= 1;
							cells[cell+1] += 1;
							//printf("Particle foes to the right\n");
	
						}
					}		
				}
				//else
					//printf("cell is NOT accepted for migration\n");
			}

			int sum_of_all_cells=0;

			for(int cell=0;cell< len_cells;cell++)
			{
				sum_of_all_cells += cells[cell];
			}

			double ave_sum_of_all_cells = (double)sum_of_all_cells / (double)N_particle;
	
			//float ave_sum_of_all_cells = (float)(sum_of_all_cells / N_particle);
			//printf("average sum of t=%d = %f\n",time_step,ave_sum_of_all_cells);
	
			sum_of_all_cells_list[time_step] = ave_sum_of_all_cells;
		}
			/* write 10 lines of text into the file stream*/

		fprintf (fp, "#Some of all cells Vs time for iteration %d \n",iter);
		for(int i = 0; i < t ;i++)
		{
			fprintf (fp, "%f\t",sum_of_all_cells_list[i]);
		}
		fprintf (fp, "\n");

	}


	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("TOTAL RUN TIME = %f s \n" ,time_spent );
	fprintf (fp,"#TOTAL RUN TIME = %f seconds \n" ,time_spent);
	/* close the file*/  
	fclose (fp);
	return 0;  
}     

#!/bin/bash
#PBS -N STEAP-S
#PBS -j eo
#PBS -l nodes=1:ppn=1
#PBS -l walltime=72:00:00
#PBS -V
cd $PBS_O_WORKDIR

module load compilers/gcc-5.1.0

./a.out >> OUT.log 2> OUTT.error


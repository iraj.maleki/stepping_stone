# Stepping Stone Model

A model for a simplified version of  the stepping stone model ([LINK](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1210594/)) (Kimura, M. and Weiss, G.H., 1964. The stepping stone model of population structure and the decrease of genetic correlation with distance. Genetics, 49(4), p.561.) 

## How to use

for use this bunch of codes please do folowing steps: 

1. Edit the folowing important parameters in the **steaping_stone_iter_V3.c** codes 

- N_particle
- t
- P_m_0
- P_0
- P_g
- Kessy
- iteration


2. Run the **run-creator.sh** script and pass it the number of folders argument. it will create some instance according to the number you passed it during the run and will submit them on the HPC cluster system as PBS job file(modify the job file based on your HPC system).
3. Run the **data-extractor.py** script for extracting all data. it will perform you mean and variance over all iteretions.

